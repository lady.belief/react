import React from 'react';
import { useState } from 'react';

function Autorization({onEnterLoginPass}) {
    const [username, setName] = useState('');
    const [password, setPass] = useState('');

    return (
        <autorization>
            <div>
                username: 
                <input
                name="username"
                onChange={(event) => setName(event.target.value)}
                />
                <br/>
                password: 
                <input
                name="password"
                type="password"
                onChange={(event) => setPass(event.target.value)}
                />
                &nbsp;&nbsp;&nbsp;
                <button onClick={() => { onEnterLoginPass({username, password});}}>
                    Enter
                </button>
            </div>
        </autorization>
    )
}

export default Autorization